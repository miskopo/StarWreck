# StarWreck :rocket:
Space - the final junkyard. This is the game about... well, guess

[![Build Status](https://travis-ci.org/miskopo/StarWreck.svg?branch=master)](https://travis-ci.org/miskopo/StarWreck) [![codecov](https://codecov.io/gh/miskopo/StarWreck/branch/master/graph/badge.svg)](https://codecov.io/gh/miskopo/StarWreck)

Game is implemented in Python using mainly pygame module.

## DISCLAIMER
This game is in its beginning phase, please be patient.
It will be fun, tho.

## You can look forward to:
* Captain John Look Pick-a-card
* Chief of security Lieutenant Wolf
* Most likely somebody else, but can't think of anyone right now


## Contributors
@miskopo @cathelyn
